package com.basic.task;

/**
 * @author Oleg Kulyk magpotash@gmail.com
 * @version 1.0
 */
public class SumNum {

    public static void main(String[] args) {

        int OddSum = 0;
        int EvenSum = 0;

        /*
         * creates a loop to show sum odd and even numbers on interval [0:100]
         */
        for (int a = 0; a <= 100; a++) {
            if (a % 2 == 0) {
                /*
                 * makes a sum of all even numbers
                 */
                EvenSum = EvenSum + a;
            } else {
                /*
                 * makes a sum of all odd numbers
                 */
                OddSum = OddSum + a;
            }

        }
        System.out.println("Sum of all odd numbers " + OddSum);
        System.out.println("Sum of all even numbers " + EvenSum);

    }
}
