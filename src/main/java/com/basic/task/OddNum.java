package com.basic.task;

/**
 * @author Oleg Kulyk magpotash@gmail.com
 * @version 1.0
 */
public class OddNum {
    public static void main(String[] args) {


        /*
         * creates a loop to show odd numbers on range from 0 till 100
         */
        for (int a = 0; a < 100; a++) {
            if (a % 2 != 0) {
                System.out.println("Odd number on interval [0;100] - " + a);
            }
        }

        /*
         * creates a loop to show even numbers on range from 0 till 100
         */
    }
}
