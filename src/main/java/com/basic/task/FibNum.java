package com.basic.task;
/**
 * @author Oleg Kulyk magpotash@gmail.com
 * @version 1.0
 * <p>
 * Imported java.util.Scanner and now users
 * can set a number into the program as they wants
 */

import java.util.Scanner;

public class FibNum {


    public static void main(String[] args) {

        int firstNumber = 1;
        int secondNumber = 1;

        int fibNums = 0;

        int maxEven = 0;
        int maxOdd = 0;

        int oddPercentage = 2;
        int evenPercentage = 0;
        float percentage = 0;
        /*
         * created Scanner to set a range of results
         */
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please, enter the size of set : ");
        int scan = scanner.nextInt();
        /*
         *Used "switch" to remove unexpected results
         */
        switch (scan) {

            case 0:
                System.out.println(0 + " if You want to see more just enter another digit.:)");
                break;
            case 1:
                System.out.println(firstNumber);
                System.out.println("Odd - 100%");
                break;
            case 2:
                System.out.println(firstNumber + " " + secondNumber + " ");
                System.out.println("Odd - 100%");
                break;

            default:
                if (scan < 0) {
                    System.out.println("Please,enter the number starts from 1....");
                } else {
                    System.out.print(firstNumber + " " + secondNumber + " ");


                    /*Now entered number in variable "scan" used
                    specially in "for" loop and counts loops.
                     * Loop used to execute Fibonacci func
                     *
                     */
                    for (int a = 3; a <= scan; a++) {

                        fibNums = firstNumber + secondNumber;
                        System.out.print(fibNums + " ");
                        firstNumber = secondNumber;
                        secondNumber = fibNums;


                        /*
                         * we using "for" loop to go through again into
                         * entered number inside of variable scan.
                         * Then using "if" to assign last even digit to
                         * MaxEven variable from variable FibNums.
                         */
                        for (int b = 0; b <= scan; b++) {

                            if (fibNums % 2 == 0) {
                                maxEven = fibNums;
                            }

                        }

                        /*
                         * we using "for" loop to go through into
                         * entered number inside of variable scan..
                         * Then using "if"  to assign last odd digit to
                         * MaxEven variable from variable FibNums.
                         */
                        for (int b = 0; b <= scan; b++) {

                            if (fibNums % 2 != 0) {
                                maxOdd = fibNums;
                            }

                        }

                        /*
                         * creates "if" statement for showing result
                         * of odd and even digits inside Fibonacci function.
                         * Assigned every odd or even result to two variables
                         * Later this result using to pop up difference
                         * between them and added in for "Percentage" var.
                         */
                        if (fibNums % 2 == 0) {

                            evenPercentage++;
                        } else {
                            oddPercentage++;
                        }
                    }
                }

                System.out.println();
                System.out.println("Max odd number - " + maxOdd);
                System.out.println("Max even number - " + maxEven);
                percentage = (float) (oddPercentage * 100) / scan;
                System.out.println("Odd - " + percentage + "%");
                percentage = (float) (evenPercentage * 100) / scan;
                System.out.println("Even - " + percentage + "%");


        }


    }


}

