package com.basic.task;

/**
 * @author Oleg Kulyk magpotash@gmail.com
 * @version 1.0
 */
public class EvenNum {
    public static void main(String[] args) {
        /*
         * creates a loop to show even numbers on range from 100 till 0
         */

        for (int a = 100; a >= 0; a--) {
            if (a % 2 == 0) {
                System.out.println("Even number on interval [0;100] - " + a);

            }

        }
    }
}
